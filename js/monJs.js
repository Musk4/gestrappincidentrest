$(function () {
    window.idMedecin = 0;


    /*-----------------------Page connexion----------------------------------*/
    $('#pageconnexion #btnconnexion').bind("click", function (e) {
        e.preventDefault();
        var mdp = $("#pageconnexion #mdp").val();
        var login = $("#pageconnexion #login").val();
        $.post("ajax/traiterconnexion.php", {
            "mdp": mdp,
            "login": login},
                foncRetourConnexion, "json");
    });

    function foncRetourConnexion(data) {
        if (data !== null) {
            $.mobile.changePage("#pageaccueil");
        } else {
            $("#pageconnexion #message").css({color: 'red'});
            $("#pageconnexion #message").html("Erreur de login et/ou mdp");
        }
    }


    /* -----------------------------Page medecins-----------------------------*/
    $("#pagemedecins #listeMedecins").on("filterablebeforefilter", function (e, data) {
        var nom = data.input.val(); // on récupère la saisie
        if (nom && nom.length >= 1) {
            $.post("ajax/traiterrecherchemedecins.php", {
                "nom": nom
            }, foncRetourRechercheMedecinPageMedecins, "json");
        }
    });

    function foncRetourRechercheMedecinPageMedecins(data) {
        console.log(data);
        var html = "";
        for (i = 0; i < data.length; i++) {
            var medecin = data[i];
            var id = medecin['id'];
            var nom = medecin['nom'];
            var prenom = medecin['prenom'];
            var adresse = medecin['adresse'];
            var tel = medecin['tel'];
            html +="<li id=" + id +"><input type='hidden' value ="+tel+">"+
                    "<a href ='#' >" + nom + "  " + prenom + "  " + adresse ;
            html +="</a></li>";
        }
        $("#pagemedecins #listeMedecins").html(html);
        $("#pagemedecins #listeMedecins").listview("refresh");
    }

    // Pour abonner un élément HTML générer dynamiquement, 
    // il faut utiliser cette syntaxe et on
    $("#pagemedecins #listeMedecins").on("click", "li", function (e) {
        var idMedecin = $(this).prop('id');
        window.idMedecin = idMedecin;
        // L'usage veut que les variables de type sélecteur commence par $
        var $medecin = $("#pagemedecins #medecin");
        $medecin.val($(this).text());
        var numeroTel = $(this).find("input:hidden").val();
        $("#panelMedecins #tel").prop("href", "tel:" + numeroTel);
        $("#panelRapports #tel").prop("href", "tel:" + numeroTel); 
        $("#pagemedecins #listeMedecins").empty();  // Vide le contenu de la liste
    });

    // Permet de vider les valeurs saisies précédemment et la liste
    $("#pagemedecins").on("pagebeforeshow", function (event, ui) {
        $("#pagemedecins #medecin").val("");
        $("#pagemedecins #listeMedecins").val("");
    });


    /*----------------------Page mise à jour médecin-------------------*/
    $("#pagemedecins #btnMajMedecin, #pagevoirlesrapports #btnMajMedecin").on("click", function (e) {
        if ($("#pagemedecins #medecin").val() == "") {
            alert("Vous devez sélectionner un médecin");
            e.preventDefault();
        } else {
            $.post("ajax/traitergetmedecin.php", {
                "idMedecin": window.idMedecin
            }, foncRetourGetMedecin, "json");
        }
    });

    function foncRetourGetMedecin(data) {
        var nom = data['nom'];
        var prenom = data['prenom'];
        var adresse = data["adresse"];
        var tel = data["tel"];
        var specialite = data["specialiteComplementaire"];
        $("#pagemajmedecin #nomprenommedecin").text(nom + " " + prenom);
        $("#pagemajmedecin #adresse").val(adresse);
        $("#pagemajmedecin #specialite").val(specialite);
        $("#pagemajmedecin #tel").val(tel);
    }

    $("#pagemajmedecin #btnEnregistrerMajMedecin").bind("click", function () {
        var adresse = $("#pagemajmedecin #adresse").val();
        var tel = $("#pagemajmedecin #tel").val();
        var specialite = $("#pagemajmedecin #specialite").val();
        $.post("ajax/traitermajmedecin.php", {
            "idMedecin": window.idMedecin,
            "adresse": adresse,
            "tel": tel,
            "specialite": specialite
        }, foncRetourMajMedecin, "json");
    });

    function foncRetourMajMedecin(data) {
        if (data == 1)
            alert("Mise à jour effectuée");
    }


    /*----------------------------Page voir les rapports------------------*/
    $("#pagemedecins #btnVoirRapports, #pagemajmedecin #btnVoirRapports").on("click", function (e) {
        if ($("#pagemedecins #medecin").val() == "") {
            alert("Vous devez sélectionner un médecin");
            e.preventDefault();
        } else {
            $.post("ajax/traitergetlesrapports.php", {
                "idMedecin": window.idMedecin
            }, foncRetourGetLesRapports, "json");
        }
    });
    
    function foncRetourGetLesRapports(data) {
        var lesRapports = data;
        var html = "";
        for (i = 0; i < lesRapports.length; i++) {
            var unRapport = lesRapports[i];
            var motif = unRapport['motif'];
            var bilan = unRapport['bilan'];
            var date = unRapport['date'];
            var nomprenom = unRapport['nom'] + "  " + unRapport['prenom'];

            html += "<tr><td>" + date + "</td><td>" + motif + "</td><td>";
            html += bilan + "</td><td>" + nomprenom + "</td></tr>";
        }
        $("#pagevoirlesrapports #listeRapports").html(html);
        $("#pagevoirlesrapports #tabRapports").table("refresh");
    }








});
